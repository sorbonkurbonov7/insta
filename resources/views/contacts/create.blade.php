@extends('home.index')

@section('content')
    <div class="container">
        {{Form::open(['route' => 'contact.store'])}}
            <div class="form-group">
                <label for="exampleInputEmail1">Phone</label>
                <input type="tel" class="form-control" id="exampleInputEmail1" name="phone_number"  placeholder="Phone">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Name</label>
                <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Name">
            </div>
            <a href="{{route('contact.index')}}" class="btn btn-default">Назад</a>
            <button type="submit" class="btn btn-success">Submit</button>
        {{Form::close()}}
    </div>
@endsection
