@extends('home.index')

@section('content')
        <div class="container">
{{--            <example-component/>--}}
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="page-header">
                        <h3>All Contacts</h3>
                        <div class="form-group">
                            <a href="{{route('contact.create')}}" class="btn btn-success">Добавить</a>
                        </div>
                    </div>
                    @forelse ($contacts as $contact)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ $contact->phone_number }}
                            </div>

                            <div class="panel-body">
                                {{ $contact->name }}
                            </div>
                            @if (Auth::check())
                                <div class="panel-footer">
                                    <favorite
                                        :contact= {{ $contact->id }}
                                            :favorited={{ $contact->favorited() ? 'true' : 'false' }}
                                    ></favorite>
                                </div>
                            @endif
                        </div>
                    @empty
                        <p>No contact created.</p>
                    @endforelse
                    {{$contacts->links()}}
                </div>
            </div>
        </div>

@endsection



