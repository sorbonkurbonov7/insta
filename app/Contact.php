<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $fillable = ['phone_number', 'name', 'user_id'];


    public static function add($fields)
    {
    	$contact = new static();
        $contact->fill($fields);
        $contact->user_id = Auth::user()->id;
        $contact->save();

        return $contact;
    }


    public function favorited()
    {
        return (bool) Favorite::where('user_id', Auth::id())
            ->where('contact_id', $this->id)
            ->first();
    }


}
